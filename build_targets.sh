#!/bin/bash

init_zephyr_repos () {
    west init -m https://github.com/zephyrproject-rtos/zephyr.git --mr main zephyrproject
    cd ./zephyrproject/
    west update --narrow --fetch-opt=--filter=tree:0
    west zephyr-export
    cd ./zephyr/
    source zephyr-env.sh
}

build_samples_hello_world () {
    sample=hello_world
    mkdir -p ../../public/${board}/${core} || true
    echo "west build -p -b ${board}/${soc}/${core} samples/${sample}"
    west build -p -b ${board}/${soc}/${core} samples/${sample}
    cat ./build/zephyr/include/generated/zephyr/version.h | grep '#' | grep BUILD_VERSION | awk '{print $3}' > ../../public/${board}/${core}/zephyr_${sample}_version.txt
    ls -lha ./build/zephyr/ | grep elf
    cp -v ./build/zephyr/zephyr.elf ../../public/${board}/${core}/zephyr_${sample}.elf
}

init_zephyr_repos

board=beaglebone_ai64
soc=j721e
core=main_r5f0_0
build_samples_hello_world

board=beaglev_fire
soc=polarfire
core=e51
build_samples_hello_world

board=beaglev_fire
soc=polarfire
core=u54
build_samples_hello_world

board=beaglev_fire
soc=polarfire
core=u54/smp
build_samples_hello_world

board=beagley_ai
soc=j722s
core=main_r5f0_0
build_samples_hello_world

board=beagley_ai
soc=j722s
core=mcu_r5f0_0
build_samples_hello_world

cd ../../
#